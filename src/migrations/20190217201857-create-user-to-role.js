module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable("user_to_roles", {
        user_id: {
            type: Sequelize.INTEGER,
            references: {
                model: "user",
                key: "id",
            },
        },
        role_id: {
            type: Sequelize.INTEGER,
            references: {
                model: "role",
                key: "id",
            },
        },
    }),
    down: queryInterface => queryInterface.dropTable("user_to_roles"),
};
