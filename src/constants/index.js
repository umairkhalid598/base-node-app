module.exports = {
    USER_STATE: {
        Verified: "VERIFIED",
        Deleted: "DELETED",
        Registered: "REGISTERED",
    },
    ROLE: {
        Admin: "ROLE_ADMIN",
        User: "ROLE_USER",
    },
};
