const jwt = require("jsonwebtoken");
const { ROLE, USER_STATE } = require("../constants/index");
const { User, Role, UserToRole } = require("../models");
const { secret } = require("../config/config")();

module.exports = {
    get: async (isUser, id) => {
        if (!isUser) {
            return User.findAll({
                include: [{
                    model: Role,
                    as: "roles",
                    where: { name: ROLE.User },
                }],
            });
        }
        return User.findAll({
            where: {
                id,
            },
            include: [{
                model: Role,
                as: "roles",
                where: { name: ROLE.User },
            }],
        });
    },
    create: async (userObj) => {
        const {
            firstName, lastName, email, password,
        } = userObj;
        const roleObj = await Role.findOne({ where: { name: ROLE.User } });
        let user = await User.findOne({ where: { email } });
        if (!roleObj) {
            const error = new Error("Role does not exist");
            error.status = 422;
            throw error;
        }
        if (!user) {
            user = await User.create({
                firstName,
                lastName,
                email,
                password,
                state: USER_STATE.Registered,
            });
        }
        await user.addRoles(roleObj);
        return User.findOne({ where: { email }, include: "roles" });
    },
    delete: async (isAdmin, id) => {
        const user = await User.findOne({
            where: {
                id,
            },
            include: "roles",
        });
        if (user.roles.length > 1) {
            UserToRole.destroy({
                where: {
                    userId: id,
                },
            });
        }
        // User.destroy({
        //     where: {
        //         id,
        //     },
        //     include: [{
        //         model: Role,
        //         as: "roles",
        //         where: { name: ROLE.User },
        //     }],
        // });
    },
    login: async (adminUserObj) => {
        const {
            email, password,
        } = adminUserObj;

        const user = await User.findOne({
            where: {
                email,
            },
            include: [{
                model: Role,
                as: "roles",
                where: { name: ROLE.User },
            }],
        });
        if (!user) {
            const error = new Error("Email does not exist");
            error.status = 401;
            throw error;
        }
        if (user.validatePassword(password)) {
            const {
                id, firstName, lastName, fullName,
            } = user;
            const token = jwt.sign(
                { sub: user.id, email, roles: user.roles.map(role => role.name) },
                secret,
            );
            return {
                id, email, firstName, lastName, fullName, token,
            };
        }
        const error = new Error("Invalid email or password");
        error.status = 401;
        throw error;
    },
};
