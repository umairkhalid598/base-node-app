const bcrypt = require("bcrypt");
const { USER_STATE } = require("../constants");

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("User", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        firstName: {
            type: DataTypes.STRING,
            field: "first_name",
        },
        lastName: {
            type: DataTypes.STRING,
            field: "last_name",
        },
        email: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false,
            validate: {
                notEmpty: { msg: "Email address cannot be empty" },
                isEmail: { msg: "Must be valid email address" },
            },
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        resetPasswordToken: {
            type: DataTypes.STRING,
            unique: true,
            field: "reset_password_token",
        },
        state: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isIn: [Object.values(USER_STATE)],
            },
        },
    }, {
        getterMethods: {
            fullName() {
                return `${this.firstName} ${this.lastName}`;
            },
        },

        setterMethods: {
            fullName(fullName) {
                const names = fullName.split(" ");

                this.setDataValue("firstName", names.slice(0, -1).join(" "));
                this.setDataValue("lastName", names.slice(-1).join(" "));
            },
            password(password) {
                this.setDataValue("password", bcrypt.hashSync(password, 10));
            },
        },
        tableName: "user",
        paranoid: true,
    });

    User.prototype.validatePassword = function (password) {
        return bcrypt.compareSync(password, this.password);
    };

    User.prototype.asJsonMap = function () {
        return {
            id: this.id,
            firstName: this.firstName,
            lastName: this.lastName,
            email: this.email,
            roles: this.roles.map(role => role.name),
        };
    };

    User.associate = (models) => {
        // associations can be defined here
        User.belongsToMany(models.Role, {
            as: "roles",
            through: "UserToRole",
            foreignKey: "userId",
        });
    };
    return User;
};
