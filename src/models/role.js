module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define("Role", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            validate: {
                notNull: true,
            },
        },
    }, {
        tableName: "role",
    });

    Role.prototype.asJsonMap = function () {
        return {
            id: this.id,
            name: this.name,
            description: this.description,
        };
    };

    Role.associate = (models) => {
        // associations can be defined here
        Role.belongsToMany(models.User, {
            as: "users",
            through: "UserToRole",
            foreignKey: "roleId",
        });
    };
    return Role;
};
