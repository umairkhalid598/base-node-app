const { body } = require("express-validator/check");

module.exports = (method) => {
    switch (method) {
    case "create":
        return [
            body("firstName", "Please provide first name")
                .exists().trim().isLength(1),
            body("lastName", "Please provide last name").trim().optional(),
            body("email", "provide valid email").trim()
                .exists().isEmail(),
            body("password", "password of minimum 6 characters").trim()
                .isLength(6),
            body("password", "password must contain one Uppercase character and one number")
                .matches(/^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]))/),
        ];
    case "login":
        return [
            body("email", "provide valid email").trim()
                .exists(),
            body("email", "provide valid email").trim()
                .isEmail(),
            body("password", "password not provided").trim()
                .exists(),
        ];
    default:
        return null;
    }
};
