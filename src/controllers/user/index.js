const { validationResult } = require("express-validator/check");
const { ROLE } = require("../../constants/index");
const userService = require("../../service/user");
const errorHandler = require("../../helper/errorHandler");

module.exports = {
    get: async (req, res) => {
        try {
            const users = await userService.get(req.user.roles[0] === ROLE.User, req.user.sub);
            res.json({ status: res.statusCode, data: users.map(admin => admin.asJsonMap()) });
        } catch (e) {
            errorHandler(e, req, res);
        }
    },
    login: async (req, res) => {
        try {
            const login = await userService.login(req.body);
            res.status(201).send(login);
        } catch (e) {
            errorHandler(e, req, res);
        }
    },
    create: async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(422).json({ status: 422, errors: errors.array() });
                return;
            }
            const user = await userService.create(req.body, ROLE.Admin);
            res.json({ status: res.statusCode, data: user.asJsonMap() });
        } catch (e) {
            errorHandler(e, req, res);
        }
    },
    getById: async (req, res) => {
        try {
            let user = await userService.get([ROLE.User].filter(e => req.user.roles.includes(e)), req.user.sub);
            if (user.length) {
                user = user[0];
            }
            res.json({ status: res.statusCode, data: user ? user.asJsonMap() : {} });
        } catch (e) {
            errorHandler(e, req, res);
        }
    },
    delete: async (req, res) => {
        try {
            await userService.delete([ROLE.User].filter(e => req.user.roles.includes(e)), Number(req.params.id));
            res.json({ status: res.statusCode, msg: "User deleted successfully" });
        } catch (e) {
            errorHandler(e, req, res);
        }
    },
};
