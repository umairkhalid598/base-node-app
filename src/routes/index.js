const role = require("./role");
const user = require("./user");

module.exports = (app) => {
    role(app);
    user(app);
};
