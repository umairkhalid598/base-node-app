const authorize = require("../helper/authorize");
const roleController = require("../controllers").role;
const validate = require("../controllers/role/validation");
const { ROLE } = require("../constants/index");

module.exports = (app) => {
    app.get("/api/roles", authorize([ROLE.Admin]), roleController.get);
    app.post("/api/roles", authorize([ROLE.Admin]), validate("create"),
        roleController.create);
};
